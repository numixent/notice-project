import React from 'react';
import App from "@components/app"

import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';


import CKEditor from '@components/common/CKEditor';

import styles from "@styles/notice.scss"
import classNames from 'classnames/bind';


import UIButton from '@components/common/UIButton'



import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

import Button from '@material-ui/core/Button';

import CustomTextField from '@components/common/CustomTextField';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import * as userActions from '@store/modules/user';


const cx = classNames.bind(styles);

class Notice extends React.Component {

  state = {        
    titleStr:'',    
    htmlStr:'',
  };

  constructor(props) {
   super(props);
  }

  test = () => {
      const { UserActions } = this.props;
      const { titleStr , htmlStr  } = this.state;      
      UserActions.sendNotice(titleStr, htmlStr, []);          
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });    
  };

  onChange = (evt) => {
    //console.log("onChange fired with event info: ",evt);
    this.setState({ htmlStr : evt });
  }

  render() {

    return (
      <App>
        
        <CustomTextField
             id="제목"              
             value={this.state.passwordinputStr}
             onChange={this.handleChange('titleStr')}
        />

         <CKEditor onChange={this.onChange} />


        {           
          <textarea
            cols="1024" rows="10" 
            disabled
            value={ this.state.htmlStr }
          />
        }


        <Button variant="outlined" color="primary"  onClick = { this.test }> 서버에 데이터 보내기 </Button>
        { 
          
            <div dangerouslySetInnerHTML={{ __html: this.state.htmlStr }}>
            </div>               
        }
      </App>
    );
  }
}

export default connect(  
  null,
  (dispatch) => ({
    UserActions: bindActionCreators(userActions, dispatch),    
  })
)(Notice);


