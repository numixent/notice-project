import styles from './CustomTextField.scss'
import classNames from 'classnames/bind'

import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const cx = classNames.bind(styles);

const CustomTextField = props => {  
  return (      
      <TextField
        required                   
        id={ props.id }
        label={ props.id }        
        value={props.value}
        onChange={props.onChange}        
        InputLabelProps={{          
          required : false,                    
          shrink: true,
        }}        
        margin="none"
        variant="outlined"
        fullWidth
        className={cx('CustomTextField','root')}        
        {...props}
      />   
  )    
}  
export default CustomTextField;
